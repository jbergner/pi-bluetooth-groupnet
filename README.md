# pi-bluetooth-groupnet

This project sets up a groupnet over bluetooth on a Raspberry Pi without a desktop environment.

The idea is to allow headless operation of a Pi, but in case it does not act as expected, to be able to establish a short-range-low-energy network connection to the device to SSH into it and to troubleshoot.
